from app.users.serializers import UserSerializer
from rest_framework import viewsets
from app.users.models import User

class ListUsers(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
